import pandas as pd
from preprocess import preprocess_corpus, config

df = pd.read_csv("2208-data.csv")
texts = list(df['text'])

new_texts = preprocess_corpus(texts, **config)

for old_text, new_text in list(zip(texts, new_texts))[:20]:
    print("Old text:", old_text)
    print("New text:", new_text)
    print("="*20)