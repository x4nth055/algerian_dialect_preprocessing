from string import ascii_letters

latin_letters  = set(ascii_letters)
arabic_letters = set("ضصثقفغعهخحجدذطكمنتالبيسشئءؤرىةوزظًٌَُإأـٍِّْْآ؟")

latin2arabic_2 = {
    "gh": "غ",
    "kh": "خ",
    "dh": "ذ",
    "th": "ث",
    "sh": "ش",
    "ch": "ش",
    "ca": "كا",
    "ga": "gا",

}

latin2arabic_1 = {
    "a": "ا",
    "b": "ب",
    "c": "س",
    "d": "د",
    "e": "ا",
    "f": "ف",
    "g": "ج",
    "h": "ه",
    "i": "ي",
    "j": "ج",
    "k": "ك",
    "l": "ل",
    "m": "م",
    "n": "ن",
    "o": "و",
    "p": "ب",
    "q": "ق",
    "r": "ر",
    "s": "س",
    "t": "ت",
    "u": "و",
    "v": "ف",
    "w": "و",
    "x": "x",
    "y": "ي",
    "z": "ز",
    "7": "ح",
    "9": "ق",
    "3": "ع",
    "5": "خ",
    "8": "غ"
}

arabic2latin_1 = { v: k for k, v in latin2arabic_1.items() }
arabic2latin_1["ا"] = "a"
arabic2latin_1["ج"] = "j"
arabic2latin_1["ب"] = "b"
arabic2latin_1["و"] = "o"
arabic2latin_1["ف"] = "f"
arabic2latin_1["ذ"] = "d"
arabic2latin_1["ش"] = "ch"
arabic2latin_1["ي"] = "i"

def convert_latin2arabic(text):
    text = list(text)
    # first convert two characters latin to one arabic letter
    # examples such as 'ch' to 'ش'
    for i in range(0, len(text)):
        part = text[i: i+2]
        joined_part = ''.join(part)
        if joined_part in latin2arabic_2:
            joined_part = latin2arabic_2[joined_part]
            part = list(joined_part)
            text[i: i+2] = part
    # convert one char to one char
    for i in range(len(text)):
        part = text[i]
        if part in latin2arabic_1:
            part = latin2arabic_1[part]
            text[i] = part

    text = ''.join(text)
    return text

def convert_arabic2latin(text):
    return ''.join([ arabic2latin_1.get(c, c) for c in text ])


def is_in_pure_latin(text):
  for c in text:
    if c in arabic_letters:
      return False
  return True
    
def is_in_pure_arabic(text):
  for c in text:
    if c in latin_letters:
      return False
  return True