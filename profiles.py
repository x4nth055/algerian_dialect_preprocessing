import pandas as pd

from preprocess import ADArabicPreprocessor, config

@profile
def preprocess_corpus_1(texts, verbose=1, **config):
    p = ADArabicPreprocessor(**config)
    new_texts = []
    for text in texts:
        new_text = p.preprocess(text)
        new_texts.append(new_text)
    if verbose:
        p.print_stats()
    return new_texts

@profile
def preprocess_corpus_2(texts, verbose=1, **config):
    p = ADArabicPreprocessor(**config)
    new_texts = [p.preprocess(text) for text in texts ]
    if verbose:
        p.print_stats()
    return new_texts

@profile
def preprocess_corpus_3(texts, verbose=1, **config):
    p = ADArabicPreprocessor(**config)
    new_texts = list(map(p.preprocess, texts))
    if verbose:
        p.print_stats()
    return new_texts

texts = list(pd.read_csv("2208-data.csv")['text'])


@profile
def main():
    preprocess_corpus_1(texts, verbose=0, **config)
    preprocess_corpus_2(texts, verbose=0, **config)
    preprocess_corpus_3(texts, verbose=0, **config)


main()

