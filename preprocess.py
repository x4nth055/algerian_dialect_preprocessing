from collections import defaultdict
import re

from sklearn.model_selection import train_test_split
from pyarabic import araby
from bs4 import BeautifulSoup # for removing HTML from text

from latin2arabic import convert_arabic2latin, convert_latin2arabic, is_in_pure_arabic, is_in_pure_latin
from emoji import emojis # for dealing with emojis
from regular_expressions import *
from stopwords import arabic_stopwords_small, arabic_stopwords_medium

seed = 1

config = {
    "to_lowercase": True,
    "strip_tatweel": True,
    "strip_tashkeel": True,
    "remove_elongation": True,
    "replace_urls_pnumbers_emails_mentions": True,
    "remove_html": True,
    "remove_emojis": False,
    "insert_spaces_between_emojis": True,
    "convert_latin_letters_to_arabic": True,
    "convert_arabic_letters_to_latin": False,
    "remove_redundant_punctuation": True,
    "remove_stopwords_small": False,
    "remove_stopwords_medium": False,
    "remove_redundant_words": True,
    "balance_dataset": False,
    "remove_no_characters_sentences": False,
    "normalize_special_arabic_chars": True,
}

# use only negative, neutral and positive
CLASSES = [1, 2, 3]

CONVERTING_CLASSES_DICT = {
    0: 1, # convert extremely negative to negative
    4: 3, # convert extremely positive to positive
}


class ADArabicPreprocessor:
    def __init__(self, **config):
        self.to_lowercase                    = config.get("to_lowercase")
        self.strip_tatweel                   = config.get("strip_tatweel")
        self.strip_tashkeel                  = config.get("strip_tashkeel")
        self.remove_elongation               = config.get("remove_elongation")
        self.replace_urls_pnumbers_emails_mentions    = config.get("replace_urls_pnumbers_emails_mentions")
        self.remove_html                     = config.get("remove_html")
        self.remove_emojis                   = config.get("remove_emojis")
        self.insert_spaces_between_emojis    = config.get("insert_spaces_between_emojis")
        self.remove_redundant_punctuation    = config.get("remove_redundant_punctuation")
        self.convert_latin_letters_to_arabic = config.get("convert_latin_letters_to_arabic")
        self.convert_arabic_letters_to_latin = config.get("convert_arabic_letters_to_latin")
        self.remove_stopwords_small          = config.get("remove_stopwords_small")
        self.remove_stopwords_medium         = config.get("remove_stopwords_medium")
        self.remove_redundant_words          = config.get("remove_redundant_words")
        self.remove_no_characters_sentences  = config.get("remove_no_characters_sentences")
        self.normalize_special_arabic_chars  = config.get("normalize_special_arabic_chars")
        
        # handle conflicts between parameters
        if self.convert_latin_letters_to_arabic and self.convert_arabic_letters_to_latin:
            print("[WARNING] Cannot set both convert_latin_letters_to_arabic and convert_arabic_letters_to_latin to True, defaulting to convert_latin_letters_to_arabic")
            self.convert_arabic_letters_to_latin = False

        if self.remove_emojis and self.insert_spaces_between_emojis:
            print("[WARNING] Cannot insert spaces between emojis if they're going to be removed, setting remove_emojis to False.")
            self.remove_emojis = False

        self._2_max_chars = 'abcdefghijklmnopqrstuvwxyzفعهخحجدشسيبلاتنمكؤروزأ0123456789!?., ىء؟آظةئذطغقثضصإ'

        self.replacing_dict = {
            "ک": "ك",
            "ہ": "ه",
            "أ": "ا",
            "إ": "ا",
            "آ": "ا",
        }
        
        # for counting affected samples during each preprocessing task
        self.affected_samples_counters = defaultdict(int)

    def preprocess(self, text):
        if self.to_lowercase:
            new_text = text.lower()
            # below block is for counting affected samples
            if new_text != text:
                self.affected_samples_counters["to_lowercase"] += 1
                text = new_text
        if self.normalize_special_arabic_chars:
            new_text = self._normalize_special_arabic_chars(text)
            if new_text != text:
                self.affected_samples_counters["normalize_special_arabic_chars"] += 1
                text = new_text
        if self.strip_tatweel:
            new_text = araby.strip_tatweel(text)
            if new_text != text:
                self.affected_samples_counters["strip_tatweel"] += 1
                text = new_text
        if self.strip_tashkeel:
            new_text = araby.strip_tashkeel(text)
            if new_text != text:
                self.affected_samples_counters["strip_tashkeel"] += 1
                text = new_text
        if self.replace_urls_pnumbers_emails_mentions:
            new_text = self._replace_urls(text)
            new_text = self._replace_phone_numbers(new_text)
            new_text = self._replace_emails(new_text)
            new_text = self._replace_mentions(new_text)
            if new_text != text:
                self.affected_samples_counters["replace_urls_pnumbers_emails"] += 1
                text = new_text
        if self.remove_elongation:
            new_text = self._remove_elongation(text)
            if new_text != text:
                self.affected_samples_counters["remove_elongation"] += 1
                text = new_text
        if self.remove_html:
            new_text = self._remove_html(text)
            if new_text != text:
                self.affected_samples_counters["remove_html"] += 1
                text = new_text
        if self.remove_emojis:
            new_text = self._remove_emojis(text)
            if new_text != text:
                self.affected_samples_counters["remove_emojis"] += 1
                text = new_text
        if self.insert_spaces_between_emojis:
            new_text = self._insert_spaces_between_emojis(text)
            if new_text != text:
                self.affected_samples_counters["insert_spaces_between_emojis"] += 1
                text = new_text
        if self.remove_redundant_punctuation:
            new_text = self._remove_redundant_punctuation(text)
            if new_text != text:
                self.affected_samples_counters["remove_redundant_punctuation"] += 1
                text = new_text
        if self.remove_stopwords_small:
            new_text = self._remove_stopwords(text, arabic_stopwords_small)
            if new_text != text:
                self.affected_samples_counters["remove_stopwords_small"] += 1
                text = new_text
        if self.remove_stopwords_medium:
            new_text = self._remove_stopwords(text, arabic_stopwords_medium)
            if new_text != text:
                self.affected_samples_counters["remove_stopwords_medium"] += 1
                text = new_text
        if self.remove_redundant_words:
            new_text = self._remove_redundant_words(text)
            if new_text != text:
                self.affected_samples_counters["remove_redundant_words"] += 1
                text = new_text
        if self.convert_latin_letters_to_arabic:
            new_text = self._convert_latin_letters_to_arabic(text)
            if new_text != text:
                self.affected_samples_counters["convert_latin_letters_to_arabic"] += 1
                text = new_text
        elif self.convert_arabic_letters_to_latin:
            new_text = self._convert_arabic_letters_to_latin(text)
            if new_text != text:
                self.affected_samples_counters["convert_arabic_letters_to_latin"] += 1
                text = new_text
        self.affected_samples_counters["total_samples"] += 1
        return text

    def _normalize_special_arabic_chars(self, text):
        for old, new in self.replacing_dict.items():
            text = text.replace(old, new)
        return text

    def _remove_redundant_words(self, text):
        new_text = ""
        d = defaultdict(int)
        for word in text.split():
            if d[word] < 2:
                new_text += word + " "
                # reset dict
                for k, _ in d.items():
                    if k != word:
                        d[k] = 0
            d[word] += 1
        return new_text.strip()

    def _remove_stopwords(self, text, stopwords_list):
        for w in stopwords_list:
            text = text.replace(f" {w} ", "").replace(f"{w} ", "").replace(f" {w}", "")
        return text

    def _remove_elongation(self, text):
        """
        Remove redundant characters (elongation) from `text` of type `str`
        Some characters are reduced to a maximum of 2, and some are reduced to maximum of 1,
        check `self._2_max_chars` and `self._1_max_chars` respectively.
        e.g:
            "هههههههه" => "هه
            "روووووح" => "رووح"
        """
        for c in self._2_max_chars:
            c3 = c*3
            c2 = c*2
            while c3 in text:
                text = text.replace(c3, c2)
        return text

    def _replace_urls(self, text):
        for regex in url_regexes:
            text = re.sub(regex, " [url] ", text)
        return text.strip()

    def _replace_phone_numbers(self, text):
        return re.sub(algerian_phone_number_regex, " [phone_number] ", text).strip()

    def _replace_emails(self, text):
        return re.sub(email_regex, " [email] ", text).strip()
    
    def _replace_mentions(self, text):
        return re.sub(user_mention_regex, " [user] ", text).strip()

    def _remove_html(self, text):
        return BeautifulSoup(text, "html.parser").get_text()

    def _remove_emojis(self, text):
        for e in emojis:
            text = text.replace(e, "")
        return text

    def _insert_spaces_between_emojis(self, text):
        for e in emojis:
            text = text.replace(e, f" {e} ")
        return text.strip()

    def _remove_redundant_punctuation(self, text):
        text_ = text
        result = re.search(redundant_punct_pattern, text)
        dif = 0
        while result:
            sub = result.group()
            sub = sorted(set(sub), key=sub.index)
            sub = " " + "".join(list(sub)) + " "
            text = "".join(
                (text[: result.span()[0] + dif], sub, text[result.span()[1] + dif :])
            )
            text_ = "".join(
                (text_[: result.span()[0]], text_[result.span()[1] :])
            ).strip()
            dif = abs(len(text) - len(text_))
            result = re.search(redundant_punct_pattern, text_)
        text = re.sub(r"\s+", " ", text)
        return text.strip()

    def _convert_latin_letters_to_arabic(self, text):
        return convert_latin2arabic(text)
    
    def _convert_arabic_letters_to_latin(self, text):
        return convert_arabic2latin(text)

    def print_stats(self):
        print("*"*20, "Affected samples for each preprocessing task", "*"*20)
        for preprocessing_task, count in self.affected_samples_counters.items():
            if preprocessing_task == "total_samples":
                continue
            percentage = count * 100 / self.affected_samples_counters["total_samples"]
            print(f"{preprocessing_task}: {count} samples - {percentage:.2f}%")



def get_balanced_dataset(texts, labels, classes, mapping_dict=None, **config):
    balance_dataset = config.get("balance_dataset")
    if not balance_dataset:
        return texts, labels    
    if mapping_dict is not None:
        classes = [mapping_dict[c] for c in classes]
    label_counts = {}
    for class_id in classes:
        label_counts[class_id] = len([l for l in labels if l == class_id])
    min_value = min(label_counts.values())
    result_label_counts = defaultdict(int)
    new_texts, new_labels = [], []
    for text, label in zip(texts, labels):
        if result_label_counts[label] >= min_value:
            continue
        new_texts.append(text)
        new_labels.append(label)
        result_label_counts[label] += 1
    return new_texts, new_labels


def remove_no_characters_samples(texts, labels, verbose=1, **config):
    remove_no_characters_sentences = config.get("remove_no_characters_sentences")
    if not remove_no_characters_sentences:
        return texts, labels
    affected_samples = 0
    new_texts, new_labels = [], []
    for text, label in zip(texts, labels):
        if not is_in_pure_arabic(text) and not is_in_pure_latin(text):
            affected_samples += 1
            continue
        else:
            new_texts.append(text)
            new_labels.append(label)
    if verbose >= 1:
        print(f"remove_no_characters_sentences: {affected_samples} samples - {affected_samples / len(texts) * 100:.2f}%")
    return new_texts, new_labels


def preprocess_corpus(texts, labels, mapping_dict=None, verbose=1, **config):
    p = ADArabicPreprocessor(**config)
    new_texts = [ p.preprocess(text) for text in texts ]
    new_texts, new_labels = get_balanced_dataset(new_texts, labels, CLASSES, mapping_dict, **config)
    new_texts, new_labels = remove_no_characters_samples(new_texts, new_labels, **config)
    if verbose:
        p.print_stats()
    return new_texts, new_labels


def process_labels(texts, labels, verbose=0):
    """
    Function to allow only labels in global `CLASSES` list and convert labels
    based on `CONVERTING_CLASSES_DICT`
    """
    labels = list(
        map(
            lambda label: label if label not in CONVERTING_CLASSES_DICT else \
            CONVERTING_CLASSES_DICT[label], 
            labels
        )
    )
    if verbose:
        print("[*] Number of samples before filtering out:", len(labels))
    new_texts, new_labels = [], []
    for text, label in zip(texts, labels):
        if label in CLASSES:
            new_texts.append(text)
            new_labels.append(label)
    if verbose:
        print("[*] Number of samples after filtering out:", len(new_labels))
    unique_classes = set(new_labels)
    mapping_dict = {}
    for i, class_ in enumerate(unique_classes):
        mapping_dict[class_] = i
    new_labels = [ mapping_dict[l] for l in new_labels ]
    return new_texts, new_labels, mapping_dict


def split_dataset(x, y, split_list):
  """
  Function to split dataset to training, validation and testing sets
  `split_list` should be a list of 3 floating points.
  e.g [0.7, 0.1, 0.2] means 70% training, 10% validation, 20% testing
  """
  # make sure split_test sums up to 1
  assert sum(split_list) == 1, "`split_list` must sum up to 1."
  X_train, X_valid, y_train, y_valid = \
    train_test_split(x, y, random_state=seed, test_size=1-split_list[0])
  X_valid, X_test, y_valid, y_test = \
    train_test_split(X_valid, y_valid, random_state=seed, 
                     test_size=split_list[2] / (split_list[2] + split_list[1]))
  return X_train, X_valid, X_test, y_train, y_valid, y_test