import nltk
from nltk.corpus import stopwords
from urllib.request import urlretrieve

# download 2 versions of stopwords for now
nltk.download("stopwords")
dest, _ = urlretrieve("https://raw.githubusercontent.com/mohataher/arabic-stop-words/master/list.txt")

arabic_stopwords_small  = stopwords.words("arabic")
arabic_stopwords_medium = open(dest).read().splitlines()

custom_stopwords = [
    "انا",
    "انت",
    "تاع",
    "فيك",
    "راك",
    "كيما",
    "ماشي",
    "راه",
    "باش",
    "بصح",
    "la",
    "ya",
    "les",
    "l",
    "de",
    "هدا",
    "اللي",
    "كنت",
    "راني",
    "علينا",
    "كاين",
    "w",
    "et",
    "عليهم",
    "هاد",
    "راهم",
    "راهي",
    "تع",
    "بلي",
    "le",
    "عندي",
    "راكم",
    "عليكم"
]